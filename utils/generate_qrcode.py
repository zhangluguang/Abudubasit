# -*- coding: utf-8 -*-
import json
import qrcode
import requests
from django.conf import settings
import os


def wx_code(access_token, data):
    url = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=" + access_token
    result = requests.post(url=url, data=json.dumps(data))
    result = result.iter_content()
    with open("wx_code.png", "wb+") as f:
        for chunk in result:
            f.write(chunk)


def qr_code(uid: str, did: str) -> str:
    url = '{"uid":%s,"did":%s}' % (uid, did)
    code = qrcode.make(url)
    name = settings.RANDOM_NAME + ".png"
    path = settings.STATICFILES_DIRS[0] + os.sep + "qrcode" + os.sep + name
    code.save(path)
    return settings.MY_URL + "qrcode/" + name


if __name__ == '__main__':
    qr_code(se=2)