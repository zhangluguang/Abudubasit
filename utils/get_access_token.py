# -*- coding: utf-8 -*-
import requests


def get_access_token(appid="wxf3c75e0c859c052a", secret="31b9f9f1f3200c2b0214db0fa6f4f737"):
    access_token_url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + \
        appid + "&secret=" + secret
    result = requests.get(url=access_token_url, timeout=60)
    result = result.json()
    token = result['access_token']
    return token


if __name__ == '__main__':
    appid = "wxf3c75e0c859c052a"
    secret = "31b9f9f1f3200c2b0214db0fa6f4f737"
    t = get_access_token(appid, secret)
    print(t)