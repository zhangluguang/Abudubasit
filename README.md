## 巴斯提
### 微信小程序后端

### 1.虚拟环境
请使用pycharm导入本项目，并使用使用pycharm自动生成虚拟环境。

### 2.第三方库
打开pycharm左下角Terminal
输入pip install -r requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple
将会自动导入依赖

### 3.运行
修改canteen\settings.py中MY_URL、APPID、SECRET等参数为自己的参数
点击运行即可

### 4.一点点介绍介绍
后端采用Django框架,数据库采用的是sqlite3