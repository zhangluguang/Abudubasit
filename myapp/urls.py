# -*- coding: utf-8 -*-
from django.conf.urls import url

from myapp.scan_code import ScanCode
from myapp.user_view import UserInfo
from myapp.seller import Order, Catalogues, IsOpen

urlpatterns = [
    url(r'^qrCode', ScanCode.as_view(), name="生成二维码"),
    url(r'^userInfo/', UserInfo.as_view(), name="用户信息"),
    url(r'^order/', Order.as_view(), name="下单"),
    url(r'^catalogue/', Catalogues.as_view(), name="点餐"),
    url(r'^isOpen', IsOpen.as_view(), name="状态"),
]
