# -*- coding: utf-8 -*-
from django.http import JsonResponse
from rest_framework import views, status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from utils.generate_qrcode import qr_code


class ScanCode(views.APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request):
        params = request.data
        desk = params.get('did', None)
        if not desk:
            return JsonResponse({"error": "缺少桌子编号"},
                                status=status.HTTP_400_BAD_REQUEST)
        return JsonResponse({"msg": qr_code(str(request.user.pk), desk)})
