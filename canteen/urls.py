"""canteen URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from utils.wechat_auth import WeChatLoginView
from myapp.seller import ShopMessage

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^login/', WeChatLoginView.as_view(), name="登录"),
    url(r'^shopMessage/', ShopMessage.as_view(), name="商店信息"),
    url('', include("myapp.urls"), name="myapp"),
]
